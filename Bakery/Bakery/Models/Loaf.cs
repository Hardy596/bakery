﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class Loaf : Bun
    {
        public Loaf(DateTime createBun)
        {
            TypeBun = TypeBun.Loaf;
            Name = "Батон";
            PrimaryPrice = 40;
            CurrentPrice = PrimaryPrice;
            CreateBun = createBun;
            ShelfLife = createBun.AddHours(48);
            SalesDeadline = createBun.AddHours(36);

            NextPrice = getNextPrice();
            ChangeTime = getChangeTime();
        }
    }
}
