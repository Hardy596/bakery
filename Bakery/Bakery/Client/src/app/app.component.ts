﻿import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { Bun } from './bun';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    providers: [DataService]
})
export class AppComponent implements OnInit {

    buns: Bun[];

    constructor(private dataService: DataService) { }

    ngOnInit() {
        setInterval(() => {
            this.loadBuns();
        }, 2000);
    }

    loadBuns() {
        this.dataService.getBuns()
            .subscribe((data: Bun[]) => this.buns = data);
    }

    refreshBuns() {
        this.dataService.refreshBuns()
            .subscribe(data => this.loadBuns());
    }
}