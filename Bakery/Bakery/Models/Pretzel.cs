﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class Pretzel : Bun
    {
        public Pretzel(DateTime createBun)
        {
            TypeBun = TypeBun.Pretzel;
            Name = "Крендель";
            PrimaryPrice = 50;
            CurrentPrice = PrimaryPrice;
            CreateBun = createBun;
            ShelfLife = createBun.AddHours(36);
            SalesDeadline = createBun.AddHours(24);

            NextPrice = getNextPrice();
            ChangeTime = getChangeTime();
        }
    }
}
