﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class Bun
    {
        public int Id { get; set; }
        public TypeBun TypeBun { get; set; }
        public string Name { get; set; }
        public double PrimaryPrice { get; set; }
        public double CurrentPrice { get; set; }
        public DateTime CreateBun { get; set; }
        public DateTime ShelfLife { get; set; }
        public DateTime SalesDeadline { get; set; }

        public double NextPrice { get; set; }
        public string ChangeTime { get; set; }
        public double getNextPrice()
        {
            double nextPrice;
            switch (TypeBun)
            {
                case TypeBun.Pretzel:
                    {
                        nextPrice = PrimaryPrice / 2;
                        break;
                    }
                case TypeBun.SourCream:
                    {
                        var chPrice = PrimaryPrice / 100 * 4;
                        nextPrice = CurrentPrice - chPrice * 2;
                        break;
                    }
                default:
                    {
                        var chPrice = PrimaryPrice / 100 * 2;
                        nextPrice = CurrentPrice - chPrice * 2;
                        break;
                    }
            }
            return Math.Round(nextPrice, 2);
        }

        public string getChangeTime()
        {
            TimeSpan chTime;
            var timeNow = DateTime.Now;
            if (TypeBun == TypeBun.Pretzel)
            {
                chTime = SalesDeadline - timeNow;
            }
            else
            {
                var difHours = CreateBun.AddHours((timeNow - CreateBun).Hours + 1);
                chTime = difHours - timeNow;
            }
            return chTime.ToString();
        }
    }


    public enum TypeBun {
        Baguette,
        Croissant,
        Loaf,
        Pretzel,
        SourCream,
    }
}
