﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class Baguette : Bun
    {
        public Baguette(DateTime createBun)
        {
            TypeBun = TypeBun.Baguette;
            Name = "Багет";
            PrimaryPrice = 60;
            CurrentPrice = PrimaryPrice;
            CreateBun = createBun;
            ShelfLife = createBun.AddHours(48);
            SalesDeadline = createBun.AddHours(24);

            NextPrice = getNextPrice();
            ChangeTime = getChangeTime();
        }
    }
}
