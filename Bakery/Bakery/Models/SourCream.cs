﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class SourCream : Bun
    {
        public SourCream(DateTime createBun)
        {
            TypeBun = TypeBun.SourCream;
            Name = "Сметанник";
            PrimaryPrice = 70;
            CurrentPrice = PrimaryPrice;
            CreateBun = createBun;
            ShelfLife = createBun.AddHours(12);
            SalesDeadline = createBun.AddHours(6);

            NextPrice = getNextPrice();
            ChangeTime = getChangeTime();
        }
    }
}
