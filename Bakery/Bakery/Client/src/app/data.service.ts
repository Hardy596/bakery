﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Bun } from './bun';

@Injectable()
export class DataService {

    private url = "/api/buns";

    constructor(private http: HttpClient) {
    }

    getBuns() {
        return this.http.get(this.url);
    }

    refreshBuns() {
        return this.http.get(this.url + '/refresh');
    }
}