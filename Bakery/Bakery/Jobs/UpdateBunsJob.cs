﻿using Bakery.Models;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Jobs
{
    [DisallowConcurrentExecution]
    public class UpdateBunsJob : IJob
    {
        private readonly IServiceProvider _serviceProvider;

        public UpdateBunsJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            using var scope = _serviceProvider.CreateScope();
            var db = scope.ServiceProvider.GetService<ApplicationContext>();

            var buns = db.Buns.ToList();
            var expiredBuns = new List<Bun>();
            var TimeNow = DateTime.Now;
            foreach (Bun bun in buns)
            {
                switch (bun.TypeBun)
                {
                    case TypeBun.Pretzel:
                        {
                            if (TimeNow == bun.SalesDeadline)
                            {
                                bun.CurrentPrice = Math.Round(bun.PrimaryPrice / 2, 2);
                            }
                            bun.ChangeTime = (bun.SalesDeadline - TimeNow).ToString();
                            break;
                        }
                    case TypeBun.SourCream:
                        {
                            var chPrice = bun.PrimaryPrice / 100 * 4;
                            bun.CurrentPrice = Math.Round(bun.CurrentPrice - chPrice, 2);
                            bun.NextPrice = Math.Round(bun.CurrentPrice - chPrice, 2);
                            var difHours = bun.CreateBun.AddHours((TimeNow - bun.CreateBun).Hours + 1);
                            bun.ChangeTime = (difHours - TimeNow).ToString();
                            break;
                        }
                    default:
                        {
                            var chPrice = bun.PrimaryPrice / 100 * 2;
                            bun.CurrentPrice = Math.Round(bun.CurrentPrice - chPrice, 2);
                            bun.NextPrice = Math.Round(bun.CurrentPrice - chPrice, 2);
                            var difHours = bun.CreateBun.AddHours((TimeNow - bun.CreateBun).Hours + 1);
                            bun.ChangeTime = (difHours - TimeNow).ToString();
                            break;
                        }
                }

                if (TimeNow.Minute == bun.ShelfLife.Minute)
                {
                    expiredBuns.Add(bun);
                }
            }

            if (expiredBuns.Count() > 0)
            {
                db.Buns.RemoveRange(expiredBuns);
            }
            db.UpdateRange(buns);

            db.SaveChanges();

            await Console.Out.WriteLineAsync("[JOB] Update Buns");
        }
    }
}
