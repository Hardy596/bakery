﻿export class Bun {
    constructor(
        public id?: number,
        public typeBun?: number,
        public name?: string,
        public primaryPrice?: number,
        public currentPrice?: number,
        public createBun?: Date,
        public shelfLife?: Date,
        public salesDeadline?: Date,

        public changeTime?: string,
        public nextPrice?: number,
    ) { }
}