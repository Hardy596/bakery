﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Models
{
    public class Croissant : Bun
    {
        public Croissant(DateTime createBun)
        {
            TypeBun = TypeBun.Croissant;
            Name = "Круассан";
            PrimaryPrice = 60;
            CurrentPrice = PrimaryPrice;
            CreateBun = createBun;
            ShelfLife = createBun.AddHours(24);
            SalesDeadline = createBun.AddHours(12);

            NextPrice = getNextPrice();
            ChangeTime = getChangeTime();
        }
    }
}
