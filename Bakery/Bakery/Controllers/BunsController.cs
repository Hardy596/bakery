﻿using Bakery.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bakery.Controllers
{
    [ApiController]
    [Route("api/buns")]
    public class BunsController : Controller
    {
        ApplicationContext db;
        DateTime dateCreateBun = DateTime.Now.AddHours(-2);
        public BunsController(ApplicationContext context)
        {
            db = context;
            if (!db.Buns.Any())
            {
                db.Buns.Add(new Baguette (dateCreateBun));
                db.Buns.Add(new Croissant (dateCreateBun));
                db.Buns.Add(new Loaf (dateCreateBun));
                db.Buns.Add(new Pretzel(dateCreateBun));
                db.Buns.Add(new SourCream(dateCreateBun));
                db.SaveChanges();
            }
        }
        [HttpGet]
        public IEnumerable<Bun> Get()
        {
            foreach(Bun bun in db.Buns)
            {
                bun.ChangeTime = bun.getChangeTime();
            }
            return db.Buns.ToList();
        }

        [HttpGet("refresh")]
        public IActionResult Refresh()
        {
            db.Buns.RemoveRange(db.Buns);

            db.Buns.Add(new Baguette(dateCreateBun));
            db.Buns.Add(new Croissant(dateCreateBun));
            db.Buns.Add(new Loaf(dateCreateBun));
            db.Buns.Add(new Pretzel(dateCreateBun));
            db.Buns.Add(new SourCream(dateCreateBun));

            db.SaveChanges();

            return Ok();
        }
    }
}
